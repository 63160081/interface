/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suphakorn.interfaceproject;

/**
 *
 * @author exhau
 */
public class Plane extends Vahicle implements Flyable,Runable{
    
    public Plane(String engine){
        super(engine);
    }

    @Override
    public void startEngine() {
        System.out.println("Plane: Start Engine");
    }

    @Override
    public void stopEngine() {
        
    }

    @Override
    public void raiseSpeed() {
        
    }

    @Override
    public void applyBreak() {
        
    }

    @Override
    public void fly() {
        System.out.println("Plane: fly");
    }

    @Override
    public void Run() {
        System.out.println("Plane: run");
    }
    
}
