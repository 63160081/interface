/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suphakorn.interfaceproject;

/**
 *
 * @author exhau
 */
public class TestFlyable {

    public static void main(String[] args) {
        Bat bat = new Bat();
        Plane plane = new Plane("Engine number 1");
        bat.fly();
        plane.fly();
        Dog dog = new Dog();

        Flyable[] flyables = {bat, plane};
        for (Flyable f : flyables) {
            if (f instanceof Plane) {
                Plane p = (Plane) f;
                p.startEngine();
                p.Run();
            }
            f.fly();
        }

        Runable[] runables = {dog, plane};
        for(Runable r : runables){
            r.Run();
        }
    }
}
